import os
import time
import re
import sys
from webbrowser import get
from bs4 import BeautifulSoup
from bs4.element import ResultSet
from selenium import webdriver
import pandas as pd
from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from Util import open_json, save_json, col, NAME_PRINCIPAL, NAME_SECUNDARIO, NAME_TERCERO, col_order
from read_captcha_pe import get_text_image


def ingresar():
    global driver
    chrome_options = Options()
    # chrome_options.add_argument('--headless')
    # chrome_options.add_argument("--disable-gpu")
    # chrome_options.add_argument('--no-sandbox')
    # chrome_options.add_argument('--disable-dev-shm-usage')
    # chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--log-level=3")

    if (sys.platform == 'linux'):
        #driver = webdriver.Chrome('chrome/chromedriver',chrome_options=chrome_options)
        driver = webdriver.Chrome(
            ChromeDriverManager().install(), chrome_options=chrome_options)
    elif (sys.platform == 'win32'):
        #driver = webdriver.Chrome('chrome/chromedriver.exe',chrome_options=chrome_options)
        driver = webdriver.Chrome(
            ChromeDriverManager().install(), chrome_options=chrome_options)
    else:
        return "No hay driver para esta plataforma"


def asignar_valores(df: pd.DataFrame):
    try:
        df_ult = pd.read_excel(os.path.join(conf["excel_path"], conf["filename"]),
                               sheet_name=NAME_PRINCIPAL)
        df_pen = pd.read_excel(os.path.join(conf["excel_path"], conf["filename"]),
                               sheet_name=NAME_SECUNDARIO)
        df_ant = pd.read_excel(os.path.join(conf["excel_path"], conf["filename"]),
                               sheet_name=NAME_TERCERO)
    except FileNotFoundError as _:
        df_ult = pd.DataFrame(columns=col)
        df_pen = pd.DataFrame(columns=col)
        df_ant = pd.DataFrame(columns=col)
    for i, row in df.iterrows():
        if conf["last_placa"] != "" and conf["last_placa"] != row["Nro_placa"]:
            continue

        try:
            res = sacar_datos(row["Nro_placa"],conf["mode"])
            if (len(res) == 0): continue
            df_ult = pd.concat([df_ult, res[0]])
            df_pen = pd.concat([df_pen, res[1]])
            df_ant = pd.concat([df_ant, res[2]])
            guardar_datos(df_ult, df_pen, df_ant)
            print(f"Placa num {i+1} guardado: {row['Nro_placa']}")
            conf["last_placa"] = ""
        except Exception as e:
            conf["last_placa"] = row["Nro_placa"]
            save_json(conf)
            driver.close()
            raise e


def sacar_datos(placa:str,mode:str):
    while True:
        try:
            driver.get(
                "http://portal.mtc.gob.pe/reportedgtt/form/frmconsultaplacaitv.aspx")
            if mode == 2:
                WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//*[@id='cboFiltroBusqueda']/option[text()='Certificado']"))).click()
            rtm: WebElement = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.ID, "txtPlaca")))
            rtm.send_keys(placa)
            time.sleep(2)
            img_catpcha: WebElement = WebDriverWait(driver, 5).until(EC.presence_of_element_located(
                (By.ID, "imgCaptcha")))
            open("captcha_pe.png", "wb").write(img_catpcha.screenshot_as_png)
            txt_catpcha = get_text_image()

            catpcha: WebElement = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.ID, "txtCaptcha")))
            catpcha.send_keys(txt_catpcha)
            time.sleep(1)
            driver.execute_script("arguments[0].click();", WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.ID, "BtnBuscar"))))
            time.sleep(2)
            try:
                pas:WebElement = WebDriverWait(driver,3)\
                    .until(EC.presence_of_element_located((By.ID,"lblAlertaMensaje")))
                if (pas.text.strip() in ["El código de chaptcha ingresado no es correcto.","No ha ingresado el código de captcha"]):
                    continue
            except (TimeoutException,NoSuchElementException) as _:
                pass
        except (TimeoutException, WebDriverException) as _:
            continue
        try:
            res = get_datos(placa)
            return res[0],res[1],res[2]
        except ValueError as e:
            print(e.args)
            if e.args[0] == "No tables found":
                return []


def guardar_datos(prin: pd.DataFrame, sec: pd.DataFrame, ter: pd.DataFrame):

    with pd.ExcelWriter(os.path.join(conf["excel_path"], conf["filename"])) as writer:
        prin.to_excel(writer, sheet_name=NAME_PRINCIPAL, index=False)
        sec.to_excel(writer, sheet_name=NAME_SECUNDARIO, index=False)
        ter.to_excel(writer, sheet_name=NAME_TERCERO, index=False)

        writer.save()


def get_datos(n_placa):
    time.sleep(1)
    datos_principales: WebElement = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
        (By.ID, "divDetalle")))
    soup = BeautifulSoup(
        datos_principales.get_attribute('innerHTML'), 'html5lib')
    try:
        tables = pd.read_html(str(soup),encoding='latin1')
        ult = pd.concat([tables[0], tables[1], tables[2]],
                        ignore_index=True, axis=1)
        ult.columns = col
        ult = ult[col_order]
    except IndexError as _:
        return pd.DataFrame([dict(zip(col_order, [n_placa]+[None]*(len(col)-1)))]), pd.DataFrame([dict(zip(col_order, [n_placa]+[None]*(len(col)-1)))]), pd.DataFrame([dict(zip(col_order, [n_placa]+[None]*(len(col)-1)))])
    try:
        pen = pd.concat([tables[3], tables[4], tables[5]],
                        ignore_index=True, axis=1)
        pen.columns = col
        pen = pen[col_order]
    except IndexError as _:
        return ult, pd.DataFrame([dict(zip(col_order, [n_placa]+[None]*(len(col)-1)))]), pd.DataFrame([dict(zip(col_order, [n_placa]+[None]*(len(col)-1)))])
    try:
        ant = pd.concat([tables[6], tables[7], tables[8]],
                        ignore_index=True, axis=1)
        ant.columns = col
        ant = ant[col_order]
        return ult, pen, ant
    except IndexError as _:
        return ult, pen, pd.DataFrame([dict(zip(col_order, [n_placa]+[None]*(len(col)-1)))])


def main(df: pd.DataFrame):
    global conf
    conf = open_json()
    ingresar()
    asignar_valores(df.astype(str))
    conf["mode"] = 0
    save_json()
