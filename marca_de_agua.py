# -*- coding: utf-8 -*-
# @Time    : 2019/11/11 16:50
# @Author  :


import cv2 as cv
import numpy as np


def console_location(path):
    """
    La posición del píxel en el área de salida de la consola
    :param path:
    :return:
    """
         # Posición fija
    img = cv.imread(path)

    def on_mouse(event, x, y, flags, param):
        if event == cv.EVENT_LBUTTONDOWN:
                         # Ancho Alto
            print(x, y)

         # Construir ventana
         # Ventana enlazada de devolución de llamada
    cv.namedWindow("img", cv.WINDOW_NORMAL)
    cv.setMouseCallback("img", on_mouse, 0)
    cv.imshow("img", img)
         # Entrada de teclado q salir
    if cv.waitKey() == ord("q"):
        cv.destroyAllWindows()


class WaterMark(object):

    def mark(self, path):
                 # Extraer área de interésROI
        img = cv.imread(path)
                 # Después de ejecutar la función console_location, haga clic en dos puntos en la imagen correspondiente para obtener los dos parámetros
                 # Alto1:alto2  anchura1:anchura2
        roi = img[248:291, 91:596]
        # cv.imwrite('02.jpg', roi)
        roi_hsv = cv.cvtColor(roi, cv.COLOR_BGR2HSV)
        # cv.imwrite('hsv.jpg', roi_hsv)

                 # Establecer blancoHSVRango
        lower = np.array([0, 0, 221])
        upper = np.array([180, 30, 247])

                 # Crea una máscara de marca de agua
        kernel = np.ones((3, 3), np.uint8)
        # cv.imwrite('kernel.jpg',kernel)
        mask = cv.inRange(roi_hsv, lower, upper)
        # cv.imwrite(r'mask.jpg',mask)
                 # Expande la máscara de marca de agua
        dilate = cv.dilate(mask, kernel, iterations=3)
        res = cv.inpaint(roi, dilate, 7, flags=cv.INPAINT_TELEA)
                 # Filtrado bilateral
        res = cv.bilateralFilter(res, 5, 280, 50)

                 # Alto1:alto2  anchura1:anchura2
        img[248:291, 91:596] = res
        cv.imwrite('modified.png', img)


if __name__ == '__main__':
    path = 'data.png'
    # console_location(path)
    w = WaterMark()
    w.mark(path)


