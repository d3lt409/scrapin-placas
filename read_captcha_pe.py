import cv2,os,sys
import pytesseract

def get_text_image():
    if sys.platform == 'win32':
        pytesseract.pytesseract.tesseract_cmd = 'C:\Program Files\Tesseract-OCR\\tesseract.exe'
    image = cv2.imread("captcha_pe.png")
    image = image[5:40,40:165]
    cv2.imwrite("captcha_pe_out.png",image)
    #os.system("convert captcha_pe_out.png -colorspace gray captcha_pe_out.png")
    os.system("convert captcha_pe_out.png -colorspace gray -threshold 60% captcha_pe_out.png")
    #os.system("convert captcha_pe_out.png -negate -morphology erode octagon:2 -negate -threshold 70% captcha_pe_out.png")
    
    #os.system("convert captcha1.png -colorspace Gray -blur 0 -level 0,60% captcha1.png")
    #os.system("convert captcha1.png -level 90%,100% captcha1.png")
    #os.system("convert captcha1.png -scale 200% -threshold 50% -morphology dilate octagon:2 captcha1.png")
    captcha:str = pytesseract.image_to_string(cv2.imread("captcha_pe_out.png"),
                                    config='--psm 8 -c tessedit_char_whitelist=1234567890')
    captcha = captcha.replace(" ", "").strip()
    return captcha