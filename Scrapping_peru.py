import os,time,re,sys
from webbrowser import get
from bs4 import BeautifulSoup
from bs4.element import ResultSet
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium import webdriver
import pandas as pd
from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from Util import  open_json, save_json,col,NAME_PRINCIPAL,NAME_SECUNDARIO,NAME_TERCERO,col_order
from read_captcha_pe import get_text_image


def ingresar():
    global driver
    chrome_options = Options()
    #chrome_options.add_argument('--headless')
    #chrome_options.add_argument("--disable-gpu")
    #chrome_options.add_argument('--no-sandbox')
    #chrome_options.add_argument('--disable-dev-shm-usage')
    #chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--log-level=3")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    chrome_options.add_experimental_option('useAutomationExtension', False)

    if (sys.platform == 'linux'):
        #driver = webdriver.Chrome('chrome/chromedriver',chrome_options=chrome_options)
        driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=chrome_options)
    elif (sys.platform == 'win32'):
        #driver = webdriver.Chrome('chrome/chromedriver.exe',chrome_options=chrome_options)
        driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=chrome_options)
    else:
        return "No hay driver para esta plataforma"
    
    set_viewport_size(driver, 500, 500)
    driver.get("https://www.sunarp.gob.pe/seccion/servicios/detalles/0/c3.html")
    driver.execute_script("arguments[0].click();",WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH, "//div[@class='jcrm-botondetalle']/a"))))
    capthcha(driver)

def set_viewport_size(driver:WebDriver, width, height):
    window_size = driver.execute_script("""
        return [window.outerWidth - window.innerWidth + arguments[0],
          window.outerHeight - window.innerHeight + arguments[1]];
        """, width, height)
    driver.set_window_size(*window_size)
    
def capthcha(driver:WebDriver):
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR,"iframe[name^='a-'][src^='https://www.google.com/recaptcha/api2/anchor?']")))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='recaptcha-anchor']"))).click()
    
ingresar()