import cv2,os,sys
import pytesseract

def get_text_image():
    if sys.platform == 'win32':
        pytesseract.pytesseract.tesseract_cmd = 'C:\Program Files\Tesseract-OCR\\tesseract.exe'
    image = cv2.imread("data.png")
    captcha:str = pytesseract.image_to_string(image,
                                    config='--psm 8')
    captcha = captcha.replace(" ", "").strip()
    return captcha

print(get_text_image())