import json
from Scraping_PE import ingresar, sacar_datos
from flask import Flask, Response, request

from Util import LABEL

app = Flask(__name__)
ingresar()


@app.route("/", methods=["POST"])
def get_data():
    body = request.get_json()
    if "modo" in body:
        if body["modo"].lower() == "placa":
            res = sacar_datos(body["value"], 1)
        else:
            res = sacar_datos(body["value"], 2)
    else:
        body["modo"] = "placa"
        res = sacar_datos(body["value"], 1)
    if len(res) > 0:
        return Response(json.dumps(
            {LABEL[i]:val.iloc[0].fillna("").to_dict() for i, val in enumerate(res)}
            ,ensure_ascii=False,indent=3), 200, mimetype='application/json')
    else:
        return Response(json.dumps(
            {"msg": f"El valor {body['value']} de tipo {body['modo'].capitalize()} no tiene datos"}), 404, mimetype='application/json')


app.run(host="127.0.0.1", port=5000)
