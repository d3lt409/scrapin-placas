import json,re

NAME_PRINCIPAL = 'ÚLTIMO REGISTRO'
NAME_SECUNDARIO = 'PENÚLTIMO REGISTRO'
NAME_TERCERO = 'ANTEPENÚLTIMO REGISTRO'
XPATH_SECUNDARIO = "//*[@id='pnlRevisionTecnicoMecanicaNacional']"
XPATH_TERCERO = "//*[@id='pnlRevisionTecnicoMecanicaNacionalOtras']"
col=["EMPRESA CERTIFICADORA","DIRECCIÓN","PLACA",	"NRO DE CERTIFICADO",	"VIGENTE DESDE",	"VIGENTE HASTA",	"RESULTADO INSPECCIÓN",	"ESTADO","ÁMBITO",	"TIPO DE SERVICIO",	"OBSERVACIONES"]
col_order=["PLACA","EMPRESA CERTIFICADORA","DIRECCIÓN",	"NRO DE CERTIFICADO",	"VIGENTE DESDE",	"VIGENTE HASTA",	"RESULTADO INSPECCIÓN",	"ESTADO","ÁMBITO",	"TIPO DE SERVICIO",	"OBSERVACIONES"]
MODE = {1:"Placa",2:"Certificado"}
LABEL = {0:"Ultima", 1:"Penultima", 2:"Antepenultima"}

def save_json(dict_file):
    path_local = open("config_pe.json","w")
    json.dump(dict_file,path_local)
    path_local.close()
    
def open_json()-> dict:
    files = open("config_pe.json")
    config = json.load(files)
    files.close()
    return config

def reemplazar_nombre(value:str):
    try:
        ver_value = re.search("(?<=\.)\w+",value.strip()).group(0)
        if ver_value not in ["xlsx","xls","xlsm"] : return ""
        return value.strip()
    except Exception as _:
        return f"{value.strip()}.xlsx"