import tkinter as tk
from tkinter import StringVar, filedialog
from tkinter import messagebox

from Util import open_json, reemplazar_nombre, save_json

class config_view:
    
    def __init__(self) -> None:
        self.root = tk.Tk()
        self.root.title("Configuración RTM")
        self.root.geometry('550x300+500+150')
        self.root.resizable(False, False)
        self.canvas1 = tk.Canvas(self.root, width = 550, height = 300, bg = 'lightgreen', relief = 'raised')
        self.canvas1.pack()
        
        self.config = open_json()
        
        label1 = tk.Label(self.root, text='Configuración RTM', bg = 'lightgreen')
        label1.config(font=('helvetica', 20))
        self.canvas1.create_window(275, 30, window=label1)
        
        label = tk.Label(self.root, text='Esta es la configuración de la ubicación del archivo \nque se va a generar al terminar la aplicación', bg = 'lightgreen')
        label.config(font=('helvetica', 12))
        self.canvas1.create_window(275, 70, window=label)
        
        self.file_path_config()
        self.file_config()
        self.save_button()
        self.exitButton()
        
        self.root.mainloop()
        
    def file_path_config(self):
        
        label = tk.Label(self.root, text='Ruta de la carpeta', bg = 'lightsteelblue2')
        label.config(font=('helvetica', 13))
        self.canvas1.create_window(75, 120, window=label)
        
        self.entry_path = tk.Entry(self.root,textvariable=StringVar(self.root, value=self.config["excel_path"]),font=('helvetica', 12, 'bold'),state='readonly',width=30)
        self.canvas1.create_window(285, 120, window=self.entry_path)
        
        self.button_entry_path = tk.Button(self.root,text="Modificar ruta", command=self.change_path, bg='blue', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(485, 120, window=self.button_entry_path)
        
    def change_path(self):
        path= filedialog.askdirectory(parent=self.root,title = "Seleccione carpeta para guardar el excel")
        if len(path) >0: 
            self.button.config(state='normal')
            self.config["excel_path"] = path
            self.entry_path.config(textvariable=StringVar(self.root, value=self.config["excel_path"]))
            
            
    def file_config(self):
        
        label = tk.Label(self.root, text='Nombre del archivo', bg = 'lightsteelblue2')
        label.config(font=('helvetica', 13))
        self.canvas1.create_window(75, 160, window=label)
        
        self.entry_name = tk.Entry(self.root,textvariable=StringVar(self.root, value=self.config["filename"]),font=('helvetica', 12, 'bold'),state='readonly',width=15)
        self.canvas1.create_window(225, 160, window=self.entry_name)
        
        self.button_entry_name = tk.Button(self.root,text="Modificar nombre del archivo", command=self.change_name_file, bg='blue', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(425, 160, window=self.button_entry_name)
        
        
    def change_name_file(self):
        self.button.config(state='normal')
        self.entry_name.config(state='normal')
        self.entry_name.focus()
        
    def save_button(self):
        self.button = tk.Button(self.root,text="      Guardar cambios     ", command=self.save_config, bg='green', fg='white', font=('helvetica', 12, 'bold'),state='disabled')
        self.canvas1.create_window(275, 230, window=self.button)
    
    def save_config(self):
        if self.config["excel_path"] == "" or self.entry_name.get() == "": 
            messagebox.showerror(parent=self.root,title= "Error de ingreso de datos",message="Debe ingresar todos los datos necesarios, no deben haber campos vacios")
            return
        self.config["filename"] = reemplazar_nombre(self.entry_name.get())
        print(self.config["filename"])
        if (self.config["filename"] == ""): 
            messagebox.showerror(parent=self.root,title= "Error de extención",message="La extención del archivo es incorrecta")
            return
        save_json(self.config)
        self.root.destroy()
    
    def exitButton(self):
        exitButton = tk.Button (self.root, text='       Salir     ',command=self.root.destroy, bg='brown', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(275, 270, window=exitButton)